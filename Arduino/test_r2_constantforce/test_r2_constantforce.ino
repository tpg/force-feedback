/*
 ForceFeedback Test: Constant force movement.
 BOARD: Rev2
 */

// To move with constant force, need:
// - A force (set with VREF)
// - A direction
// Always want the energised coils to be pulling one way
// - When it gets too close to the target position, swap upwards
// - When too far way, swap closer.
// NOTE: Want to track the coil phase (position and output) internally, so that if there's slip 
// the sign can be recovered
// - Encoder `count/10` gives the step index, there's four phases to the steps (A+, B+, A-, B-)
// - Stepper will switch through these too.
// - To maintain constant force sign, we want the stepper's internal phase to be ahead/behind the position phase.


// Starting from a zero rest state
// - Set the coils ahead
//   > When count goes below -5, need to switch backwards one step (pulling towards zero)
//   > When count goes above 5, switch forwards (pushing positive)

// STATUS:
// - Can end up slightly offset from the proper center.
// - Sometimes stops when it shouldn't :(
// IDEA: In final build, have a self-calibration routine that moves the motor with no load to calibrate/reset state

// NOTE:
// - Encoder is 1000 pulse (4000 counts) per revolution
// - Stepper is 400 steps per resolution
// > So, expect 10 counts (2.5 cycles) per stepper step.
// > Step the motor when a change of more than 10 is seen?

// NOTE: DIR=0 is positive encoder (on CH1)

#define INPUT_ENC1A 2
#define INPUT_ENC1B 3
#define OUTPUT_VREF1  5 // D5
#define OUTPUT_VREF2  6 // D6
const int OUTPUT_MOT_ON = 9;
const int OUTPUT_DIR1 = 12;
const int OUTPUT_CLK1 = 13;
const int INPUT_VREF1 = 15;  // A1
const int INPUT_POT1 = 18;

volatile int val_1; // Encoder counter
volatile int val_1_raw; // Encoder counter (not clamped)

signed char ctrl_phase_1;  // Current "phase" of the controller (0-3)
signed char axle_phase_1;  // Current "phase" of the axle (0-3)

// Addition modulo 4
static signed char phase_add(signed char v, signed char i) {
  v += i;
  while(v > 3)  v -= 4;
  while(v < 0)  v += 4;
  return v;
}

// Step channel once in the given direction
void step_once_1(int dir) {
  ctrl_phase_1 = phase_add(ctrl_phase_1, dir < 0 ? -1 : 1);
  
  digitalWrite(OUTPUT_DIR1, dir < 0);
  delayMicroseconds(1);
  digitalWrite(OUTPUT_CLK1, 1);
  delayMicroseconds(1);
  digitalWrite(OUTPUT_CLK1, 0);
  delayMicroseconds(1);
}

void setup()
{
  Serial.begin(9600);
  pinMode(OUTPUT_VREF1, OUTPUT); 
  pinMode(OUTPUT_MOT_ON, OUTPUT);
  pinMode(OUTPUT_DIR1, OUTPUT);
  pinMode(OUTPUT_CLK1, OUTPUT);

  init_isr_enc1();
  
  // Set output current to 50%
  analogWrite(OUTPUT_VREF1, 128);
  analogWrite(OUTPUT_VREF2, 0);
  delay(5);
  // Read it back
  Serial.print("VRef1 = ");
  Serial.print(analogRead(INPUT_VREF1));
  Serial.println("");
  // Turn controllers on
  digitalWrite(OUTPUT_MOT_ON, 1);
  
  // Wait some time
  delay(500);
  // Reset the counters
  val_1 = 0;
}

int step_count;  // Number of steps performed since last log
int last_val1;  // Last value of `val_1` seen
int err_count;  // Number of times the phases have been out by 180 degrees
int loop_count;
int backdrive_count;
int TARGET_DIR = 1;
int s_run = 1;
int last_seen_encoder;
void loop()
{
  int should_log = 0;
  // Serial input
  if( Serial.available() ) {
    int b = Serial.read();
    int upd = 1;
    switch(b)
    {
    case 'w': axle_phase_1 = phase_add(axle_phase_1,  1); break;
    case 's': axle_phase_1 = phase_add(axle_phase_1, -1); break;
    case 'a': TARGET_DIR = (TARGET_DIR >= 0 ?  1 : 0); break;
    case 'd': TARGET_DIR = (TARGET_DIR <= 0 ? -1 : 0); break;
    case 'p': s_run = s_run ? 0 : 1; break;
    //case 'o': s_run = !s_run; break;
    case ' ': break;
    default:
      upd = 0;
      break;
    }
    if(upd) {
      Serial.print("axle_phase_1="); Serial.print(axle_phase_1);
      Serial.print(" TARGET_DIR=");
      Serial.println(TARGET_DIR);
      should_log = 1;
    }
  }
  
  
  // Monitor the encoder value, and see if it exceeds +/- 5
  // - When it does, adjust back into range and update the current axle phase
  int v0 = val_1;
  int v = v0;
  while( v <= -6 ) {
    val_1 += 10;
    v     += 10;
    axle_phase_1 = phase_add(axle_phase_1, -1);
    //should_log = 1;
  }
  while( v >= 6 ) {
    val_1 -= 10;
    v     -= 10;
    axle_phase_1 = phase_add(axle_phase_1, +1);
    //should_log = 1;
  }
  // In-range

  // Has the encoder moved?
  if( last_seen_encoder != v0 ) {
    last_seen_encoder = v0;
    if( !s_run ){
      should_log = true;
    }
  }
  
  // Compare `ctrl_phase_1` and `real_phase_1` to determine the current "mode"
  // - Equal: The system is now holding, we want to push `ctrl` ahead/behind `real` (depending on target direction)
  // - +/- One: Either pulling the wrong way (need to to step twice), or pulling the right way (no change)
  // - Opposite: Bad - unstable version of holding configuration. Step once to get direction going the right way again.
  
  // Get a target `ctrl_phase_1` based on `real_phase_1` and a target direction
  signed char target_ctrl_phase_1 = phase_add(axle_phase_1, TARGET_DIR);
  
  signed char cur_ctrl_phase_1 = ctrl_phase_1;
  signed char phase_diff = phase_add(cur_ctrl_phase_1, -target_ctrl_phase_1);
  if( s_run == 0 ) {
    // Not running, don't do any steps
  }
  else if( phase_diff == 0 ) {
    // No need to change
  }
  else if( phase_diff == 3 ) {
    // Need to shift phase up
    if( TARGET_DIR < 0 ) {
      backdrive_count += 1;
    }
    step_once_1(1);
    step_count += 1;
  }
  else if( phase_diff == 1 ) {
    // Need to shift phase down
    if( TARGET_DIR > 0 ) {
      backdrive_count += 1;
    }
    step_once_1(-1);
    step_count += 1;
  }
  else {
    // Going opposite! - Run twice
    //step_once_1(1);
    //step_once_1(1);
    //step_count += 2;
    err_count += 1;
  }
  
  loop_count += 1;
  if(loop_count > 30000) {
    loop_count = 0;
    should_log = true;
  }
  
  // TODO: Keep track of state to be printed (and only print if there's buffer space)
  // - Current phase values
  // - Total number of steps taken since last log
  // - Number of times the phases have ended up opposed
  if( /*step_count > 0 ||*/ err_count > 0 || should_log /*|| phase_diff != 0*/ ) {
    if( true || !s_run /*Serial.availableForWrite() > 20*/ ) {
      Serial.print(step_count);
      Serial.print(" ");
      Serial.print(last_seen_encoder);
      Serial.print(" E="); Serial.print(err_count);
      Serial.print(" B="); Serial.print(backdrive_count);
      Serial.print(" A:"); Serial.print(axle_phase_1);
      Serial.print(" C:"); Serial.print(cur_ctrl_phase_1);
      Serial.print("-"); Serial.print(target_ctrl_phase_1);
      Serial.print("="); Serial.print(phase_diff);
      Serial.print(" D="); Serial.print(TARGET_DIR);
      Serial.print(" R="); Serial.print(s_run);
      Serial.print(" "); Serial.print(val_1_raw);
      Serial.println("");
      step_count = 0;
      err_count = 0;
    }
  }
}

const char E = -99;
// Mapping of transitions of the two encoder state bits to relative encoder counts
// - low bits are current value
// - high bits are previous value
const char GREY_MAPPING[16] = {
   0, -1,  1,  E, // LL -> {LL, LH, HL, HH}
   1,  0,  E, -1, // LH -> {LL, LH, HL, HH}
  -1,  E,  0,  1, // HL -> {LL, LH, HL, HH}
   E,  1, -1,  0, // HH -> {LL, LH, HL, HH}
};


uint8_t pinmask_ENC1A;
uint8_t pinmask_ENC1B;
volatile uint8_t *pinport_ENC1A;
volatile uint8_t *pinport_ENC1B;
void init_isr_enc1() {
  pinmask_ENC1A = digitalPinToBitMask(INPUT_ENC1A);
  pinmask_ENC1B = digitalPinToBitMask(INPUT_ENC1B);
  pinport_ENC1A = portInputRegister(digitalPinToPort(INPUT_ENC1A));
  pinport_ENC1B = portInputRegister(digitalPinToPort(INPUT_ENC1B));
  attachInterrupt(0, isr_enc1, CHANGE);
  attachInterrupt(1, isr_enc1, CHANGE);

  //Serial.print("pinmask_ENC1A = "); Serial.print(pinmask_ENC1A); Serial.print(" ");
  //Serial.print("pinport_ENC1A = "); Serial.print((uint32_t)pinport_ENC1A,HEX); Serial.println("");
  //Serial.print("pinmask_ENC1B = "); Serial.print(pinmask_ENC1B); Serial.print(" ");
  //Serial.print("pinport_ENC1B = "); Serial.print((uint32_t)pinport_ENC1B,HEX); Serial.println("");
}

// Encoder edge interrupt
// Update the counters based on the transition type (see GREY_MAPPING)
volatile char last_1;
void isr_enc1() {
  uint8_t pinValue_ENC1A = (*pinport_ENC1A & pinmask_ENC1A) != 0;
  uint8_t pinValue_ENC1B = (*pinport_ENC1B & pinmask_ENC1B) != 0;
  
  char v = ((char)pinValue_ENC1A << 1) + (char)pinValue_ENC1B;
  if( v != last_1 ) {
    char adj = GREY_MAPPING[ (char)(last_1 << 2) + v ];
    if(adj == E) {
    }
    else {
      val_1 += adj;
      val_1_raw += adj;
    }
    last_1 = v;
  }
}
