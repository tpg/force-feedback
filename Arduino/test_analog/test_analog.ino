/*
 TEST: DAC with feedback.
 */

const int PIN_CHA_O_VRF = 3;
const int PIN_CHB_O_VRF = 6;
const int PIN_CHA_I_VRF = 16; //A2;
const int PIN_CHB_I_VRF = 19; //A5
const int PIN_LED = 13;  // Orange LED

void setup()
{
  Serial.begin(9600);
  pinMode(PIN_CHA_O_VRF, OUTPUT);	// PWM
  pinMode(PIN_CHB_O_VRF, OUTPUT);	// PWM
  pinMode(PIN_LED, OUTPUT);   
}

int step_val;
int cnt;
void loop()
{
  Serial.print(step_val);
  Serial.print(" ");
  analogWrite(PIN_CHB_O_VRF, step_val);
  analogWrite(PIN_CHA_O_VRF, step_val);
  delay(50);
  Serial.print(analogRead(PIN_CHA_I_VRF));
  Serial.print(" ");
  Serial.print(analogRead(PIN_CHB_I_VRF));
  Serial.print(",");
  delay(50);
  Serial.print(analogRead(PIN_CHA_I_VRF));
  Serial.print(" ");
  Serial.print(analogRead(PIN_CHB_I_VRF));
  Serial.println("");
  
  cnt += 1;
  if( cnt > 1 )
  {
    cnt = 0;
    step_val += 1;
    if( step_val == 256 ) {
      step_val = 0;
    }
  }
  digitalWrite(PIN_LED, (step_val & 1) ? HIGH : LOW);
  delay(100);
}

