/*
 ForceFeedback main
 */
const int PIN_CHA_O_DIR = 2; //D2;
const int PIN_CHA_O_VRF = 3; //D3;
const int PIN_CHA_O_CLK = 4; //D4;
const int PIN_CHB_O_DIR = 5; //D5;
const int PIN_CHB_O_VRF = 6; //D6;
const int PIN_CHB_O_CLK = 7; //D7;
const int PIN_CHA_I_POT = 14; //A0;
const int PIN_CHA_I_ENC = 15; //A1;
const int PIN_CHA_I_VRF = 16; //A2;
const int PIN_CHB_I_POT = 17; //A3;
const int PIN_CHB_I_ENC = 18; //A4;
const int PIN_CHB_I_VRF = 19; //A5;
const int PIN_LED = 13;  // Orange LED

int prev_encoder_a;
int prev_encoder_b;
int v;  // Temporary
int counter;
int raw_a;
int prev_raw_a;

void setup()
{
  Serial.begin(9600);
  pinMode(PIN_CHA_O_DIR, OUTPUT);
  pinMode(PIN_CHA_O_VRF, OUTPUT);	// PWM
  pinMode(PIN_CHA_O_CLK, OUTPUT);
  pinMode(PIN_CHB_O_DIR, OUTPUT);
  pinMode(PIN_CHB_O_VRF, OUTPUT);	// PWM
  pinMode(PIN_CHB_O_CLK, OUTPUT);
  pinMode(PIN_LED, OUTPUT);   
}

void loop()
{
  if(true) {
    raw_a = analogRead(PIN_CHA_I_ENC);
    if( abs(raw_a - prev_raw_a) > 10 ) {
      Serial.print("A "); Serial.print(raw_a); Serial.println("");
      prev_raw_a = raw_a;
    }
  }
  if( false ) {
    raw_a = analogRead(PIN_CHA_I_ENC);
  v = map(raw_a, 0, 1024, 0, 15);
  if( v != prev_encoder_a ) {
    Serial.print("A ");
    Serial.print(prev_encoder_a);
    Serial.print(" -> ");
    Serial.print(v);
    Serial.print(": ");
    Serial.print(raw_a);
    Serial.println("");
    prev_encoder_a = v;
  }
  }

  if(false) {
  v = map(analogRead(PIN_CHB_I_ENC), 0, 1023, 0, 15);
  if( v != prev_encoder_b ) {
    Serial.print("B ");
    Serial.print(prev_encoder_b);
    Serial.print(" -> ");
    Serial.println(v);
    prev_encoder_b = v;
  }
  }
  
  // Blink a LED at 500ms/500ms
  counter += 1;
  if( counter == 50 ) {
    digitalWrite(PIN_LED, HIGH);
  }
  if(counter >= 100) {
    digitalWrite(PIN_LED, LOW);
    counter = 0;
    if(false) {
    Serial.print("RAW ");
    Serial.print(analogRead(PIN_CHA_I_ENC));
    Serial.print(" ");
    Serial.print(analogRead(PIN_CHB_I_ENC));
    Serial.println("");
    }
  }
  delay(10);
}

