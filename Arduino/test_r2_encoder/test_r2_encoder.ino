/*
 ForceFeedback Test: Encoder counter
 BOARD: Rev2
 */
// #include "../board_v2.h"
const int INPUT_ENC1A = 2;
const int INPUT_ENC1B = 3;
const int INPUT_ENC2A = 4;
const int OUTPUT_VREF1 = 5;  // D5
const int OUTPUT_VREF2 = 6;  // D6
const int INPUT_ENC2B = 7;
const int OUTPUT_MOT_ON = 9;
const int OUTPUT_DIR2 = 10;
const int OUTPUT_CLK2 = 11;
const int OUTPUT_DIR1 = 12;
const int OUTPUT_CLK1 = 13;
const int INPUT_VREF2 = 14;  // A0
const int INPUT_VREF1 = 15;  // A1
const int INPUT_POT1 = 18;
const int INPUT_POT2 = 19;
const int PIN_LED = 13;  // Orange LED

void setup()
{
  Serial.begin(9600);
  pinMode(OUTPUT_VREF1, OUTPUT);
  pinMode(OUTPUT_VREF2, OUTPUT);
  pinMode(OUTPUT_MOT_ON, OUTPUT);
  pinMode(OUTPUT_DIR2, OUTPUT);
  pinMode(OUTPUT_CLK2, OUTPUT);
  pinMode(OUTPUT_DIR1, OUTPUT);
  pinMode(OUTPUT_CLK1, OUTPUT);
  pinMode(PIN_LED, OUTPUT);
  
  delay(5);
  digitalWrite(OUTPUT_MOT_ON, 1);
  delay(100);
  
  attachInterrupt(0, isr_enc1, CHANGE);
  attachInterrupt(1, isr_enc1, CHANGE);
}

int v;
int last_1, val_1;
int last_2, val_2;
const int E = -99;
const int GREY_MAPPING[16] = {
  0, -1, 1, E,
  1, 0, E, -1,
  -1, E, 0, 1,
  E, 1, -1, 0,
};

int err_count_1;
void isr_enc1() {
  int v = digitalRead(INPUT_ENC1A) * 2 + digitalRead(INPUT_ENC1B);
  if( v != last_1 ) {
    // Count on rising edge of [A], depending on sign of [B]
    int adj = GREY_MAPPING[last_1 * 4 + v];
    if(adj == E) {
      err_count_1 += 1;
    }
    else {
      val_1 += adj;
    }
    last_1 = v;
  }
}
int last_seen_1;
int step_count;
long count;
void loop()
{
  if( val_1 >= 4000 ) {
    val_1 -= 4000;
  }
  if( val_1 < 0 ) {
    val_1 += 4000;
  }
  if( val_1 != last_seen_1 ) {
    Serial.print("Encoder 1: ");
    int v = val_1;
    Serial.print(last_seen_1);
    Serial.print("->");
    Serial.print(v);
    Serial.print(" (EC="); Serial.print(err_count_1); Serial.print(")");
    Serial.print(step_count);
    Serial.println("");
    last_seen_1 = v;
  }
  
  v = digitalRead(INPUT_ENC2A) * 2 + digitalRead(INPUT_ENC2B);
  if( v != last_2 ) {
    int diff = v  ^ last_2;
    last_2 = v;
    switch(diff)
    {
    case 0: break;
    case 1: val_2 += 1; break;
    case 2: val_2 -= 1; break;
    case 3: break;
    }
    Serial.print("Encoder 2: "); Serial.println(val_2);
  }
  count += 1;
  if( count > 80000 ) {
    count = 0;
    step_count += 1;
    digitalWrite(OUTPUT_CLK1, 1);
    delay(1);
    digitalWrite(OUTPUT_CLK1, 0);
  }
}
