/*
 ForceFeedback Test: Stepper movement
 */
const int PIN_CHA_O_DIR = 2; //D2;
const int PIN_CHA_O_VRF = 3; //D3;
const int PIN_CHA_O_CLK = 4; //D4;
const int PIN_CHB_O_DIR = 5; //D5;
const int PIN_CHB_O_VRF = 6; //D6;
const int PIN_CHB_O_CLK = 7; //D7;
const int PIN_CHA_I_POT = 14; //A0;
const int PIN_CHA_I_ENC = 15; //A1;
const int PIN_CHA_I_VRF = 16; //A2;
const int PIN_CHB_I_POT = 17; //A3;
const int PIN_CHB_I_ENC = 18; //A4;
const int PIN_CHB_I_VRF = 19; //A5;
const int PIN_LED = 13;  // Orange LED

void setup()
{
  Serial.begin(9600);
  pinMode(PIN_CHA_O_DIR, OUTPUT);
  pinMode(PIN_CHA_O_VRF, OUTPUT);	// PWM
  pinMode(PIN_CHA_O_CLK, OUTPUT);
  pinMode(PIN_CHB_O_DIR, OUTPUT);
  pinMode(PIN_CHB_O_VRF, OUTPUT);	// PWM
  pinMode(PIN_CHB_O_CLK, OUTPUT);
  pinMode(PIN_LED, OUTPUT);   
  
  digitalWrite(PIN_CHA_O_DIR, LOW);
}


void loop()
{
  digitalWrite(PIN_CHA_O_CLK, HIGH);
    digitalWrite(PIN_LED, HIGH);
  delay(100);
  digitalWrite(PIN_CHA_O_CLK, LOW);
    digitalWrite(PIN_LED, LOW);
  delay(100);
}
