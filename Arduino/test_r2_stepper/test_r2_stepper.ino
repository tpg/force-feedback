/*
 ForceFeedback Test: Stepper moving with encoder
 BOARD: Rev2
 */
// #include "../board_v2.h"
const int INPUT_ENC1A = 2;
const int INPUT_ENC1B = 3;
const int INPUT_ENC2A = 4;
const int OUTPUT_VREF1 = 5;  // D5
const int OUTPUT_VREF2 = 6;  // D6
const int INPUT_ENC2B = 7;
const int OUTPUT_MOT_ON = 9;
const int OUTPUT_DIR2 = 10;
const int OUTPUT_CLK2 = 11;
const int OUTPUT_DIR1 = 12;
const int OUTPUT_CLK1 = 13;
const int INPUT_VREF2 = 14;  // A0
const int INPUT_VREF1 = 15;  // A1
const int INPUT_POT1 = 18;
const int INPUT_POT2 = 19;
const int PIN_LED = 13;  // Orange LED


const int E = -99;
const int GREY_MAPPING[16] = {
  0, -1, 1, E,
  1, 0, E, -1,
  -1, E, 0, 1,
  E, 1, -1, 0,
};
int last_1, val_1;
int err_count_1;

void setup()
{
  Serial.begin(9600);
  pinMode(OUTPUT_VREF1, OUTPUT);
  pinMode(OUTPUT_VREF2, OUTPUT);
  pinMode(OUTPUT_MOT_ON, OUTPUT);
  pinMode(OUTPUT_DIR2, OUTPUT);
  pinMode(OUTPUT_CLK2, OUTPUT);
  pinMode(OUTPUT_DIR1, OUTPUT);
  pinMode(OUTPUT_CLK1, OUTPUT);
  pinMode(PIN_LED, OUTPUT);
  
  attachInterrupt(0, isr_enc1, CHANGE);
  attachInterrupt(1, isr_enc1, CHANGE);
  
  digitalWrite(OUTPUT_MOT_ON, 1);
  digitalWrite(OUTPUT_DIR1, 0);
  delay(100);
  val_1 = 0;
}

void isr_enc1() {
  int v = digitalRead(INPUT_ENC1A) * 2 + digitalRead(INPUT_ENC1B);
  if( v != last_1 ) {
    // Count on rising edge of [A], depending on sign of [B]
    int adj = GREY_MAPPING[last_1 * 4 + v];
    if(adj == E) {
      err_count_1 += 1;
    }
    else {
      val_1 += adj;
    }
    last_1 = v;
  }
}
int last_seen_1;
int delay_count;
void loop()
{
  if( val_1 != last_seen_1 ) {
    Serial.print("Encoder 1: ");
    int v = val_1;
    Serial.print(last_seen_1);
    Serial.print("->");
    Serial.print(v);
    Serial.print(" (EC="); Serial.print(err_count_1); Serial.print(")");
    Serial.println("");
    last_seen_1 = v;
  }
  
  // Shift the motor every 1s
  if( delay_count > 100 ) {
    delay_count = 0;
    digitalWrite(OUTPUT_CLK1, 1);
    delay(1);
    digitalWrite(OUTPUT_CLK1, 0);
  }
  delay(10);
  delay_count += 1; 
}
