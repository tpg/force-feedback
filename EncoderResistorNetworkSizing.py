
# E12
# 10 12 15 18 22 27 33 39 47 56 68 82

#values = [10, 22, 39, 82,]
values = [82, 39, 22, 10]

class Vals(object):
	def __init__(self):
		self.hi = []
		self.lo = []
	def push(self, is_high, v):
		# Assumes either tying high, or tying low
		if is_high:
			self.hi.append(v)
		else:
			self.lo.append(v)
	def clone(self):
		rv = Vals()
		rv.hi = [v for v in self.hi]
		rv.lo = [v for v in self.lo]
		return rv
	def calc(self):
		if len(self.hi) == 0:
			return 0.0
		if len(self.lo) == 0:
			return 1.0
		hi = 1.0 / sum(( 1.0 / v for v in self.hi))
		lo = 1.0 / sum(( 1.0 / v for v in self.lo))
		return lo / (hi + lo)
	def __repr__(self):
		return "Vals({!r},{!r})".format(self.hi, self.lo)

prev_max = 0
for state in range(16):
	#output_vals = [0.0]
	#for i,r in enumerate(values):
	#		ov = []
	#		for v in output_vals:
	#			for m in [0.95,1.05]:
	#				R = r * m
	#				ov.append( v + R / (R + 10) )
	#		output_vals = ov
	output_vals = [Vals()]
	for i,r in enumerate(values):
		is_high = ((state >> i) & 1 != 0)
		ov = []
		for v in output_vals:
			for m in [0.95,1.0,1.05]:
				ov.append(v.clone())
				ov[-1].push(is_high, r*m)
		output_vals = ov
	#print(output_vals)
	output_vals = [v.calc() for v in output_vals]

	#print(state, "{:.3f} -- {:.3f}".format(min(output_vals), max(output_vals)))
	output_vals = [ round(v * (4./5.)*1023) for v in output_vals ]
	margin = min(output_vals) - prev_max
	print("{:2} {:4.0f} -- {:4.0f} ({:4.0f})".format(state, min(output_vals), max(output_vals), margin))
	if min(output_vals) < prev_max:
		print("OVERLAP")
	prev_max = max(output_vals)

